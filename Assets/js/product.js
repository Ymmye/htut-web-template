// Step 1: Get DOM elements
const nextDom = document.getElementById("next");
const prevDom = document.getElementById("prev");
const carouselDom = document.querySelector(".carousel");
const sliderDom = carouselDom.querySelector(".list");
const thumbnailBorderDom = document.querySelector(".carousel .thumbnail");
const thumbnailItemsDom = thumbnailBorderDom.querySelectorAll(".item");
const timeDom = document.querySelector(".carousel .time");

// Move the first thumbnail to the end to match initial slide
thumbnailBorderDom.appendChild(thumbnailItemsDom[0]);

// Timing configuration
const timeRunning = 3000;
const timeAutoNext = 7000;

// Click event listeners for next and previous buttons
nextDom.onclick = function () {
  showSlider("next");
};

prevDom.onclick = function () {
  showSlider("prev");
};

// Set up initial automatic slide change
let runNextAuto = setTimeout(() => {
  nextDom.click();
}, timeAutoNext);

// Function to handle showing the next or previous slide
function showSlider(type) {
  const sliderItemsDom = sliderDom.querySelectorAll(".item");

  if (type === "next") {
    sliderDom.appendChild(sliderItemsDom[0]);
    thumbnailBorderDom.appendChild(thumbnailItemsDom[0]);
    carouselDom.classList.add("next");
  } else {
    sliderDom.prepend(sliderItemsDom[sliderItemsDom.length - 1]);
    thumbnailBorderDom.prepend(thumbnailItemsDom[thumbnailItemsDom.length - 1]);
    carouselDom.classList.add("prev");
  }

  // Clear transition class after a delay
  setTimeout(() => {
    carouselDom.classList.remove("next", "prev");
  }, timeRunning);

  // Reset automatic slide change timer
  clearTimeout(runNextAuto);
  runNextAuto = setTimeout(() => {
    nextDom.click();
  }, timeAutoNext);
}
