// ---------------------------modal box-----------

// Get the logo element
var logo = document.getElementById("logo");

// Add click event listener to the logo
logo.addEventListener("click", function () {
  window.location.href = "index.html"; // Redirect to index.html
});

// Get the modal
var modal = document.getElementById("myModal");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks on the icon, open the modal
document
  .querySelector(".ri-send-plane-fill")
  .addEventListener("click", function () {
    modal.style.display = "block";
  });

// When the user clicks on <span> (x), close the modal
span.onclick = function () {
  modal.style.display = "none";
};

// When the user clicks anywhere outside of the modal, close it
window.onclick = function (event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
};

// Get the <i> element for the send plane icon
var sendPlaneIcon = document.getElementById("ri-send");

// When the user clicks on the send plane icon, add the 'clicked' class to trigger the animation
sendPlaneIcon.addEventListener("click", function () {
  sendPlaneIcon.classList.add("clicked");

  // After the animation duration, remove the 'clicked' class to reset the animation
  setTimeout(function () {
    sendPlaneIcon.classList.remove("clicked");
  }, 300); // Adjust the timeout value to match the animation duration
});

// ------------------------------------

var swiper = new Swiper(".mySwiper", {
  slidesPerView: 1.5,
  centeredSlides: true,
  spaceBetween: 30,
  autoplay: {
    delay: 3500,
    disableOnInteraction: false,
  },
  pagination: {
    el: ".swiper-pagination",
    clickable: true,
  },
  navigation: {
    nextEl: ".swiper-button-next",
    prevEl: ".swiper-button-prev",
  },
  loop: true,
});

var menu = document.querySelector(".ri-menu-3-line");
var clo = document.querySelector(".ri-close-fill");
var nav = document.querySelector(".respo-nav");
var thumbnail = document.querySelector(".thumbnail");
var arrows = document.querySelector(".arrows");
var time = document.querySelector(".time");

menu.addEventListener("click", function () {
  nav.style.top = "0%";
  thumbnail.style.display = "none";
  arrows.style.display = "none";
  time.style.display = "none";
});

clo.addEventListener("click", function () {
  nav.style.top = "-120%";
  thumbnail.style.display = "flex";
  arrows.style.display = "flex";
  time.style.display = "flex";
});

var store = document.querySelector("#store");
var subMenu = document.querySelector(".submenu");

store.addEventListener("mousemove", function () {
  subMenu.style.top = "5%";
});

store.addEventListener("mouseleave", function () {
  subMenu.style.top = "-100%";
});
